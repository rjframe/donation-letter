# Donation Letter Generator

Note that this is not yet usable.

I recently discovered that generating end-of-year tax letters was a
surprisingly manual process at a few local non-profit organizations. Those
that wish to list individual contributions in addition to the total
contribution amount were creating each letter by hand; Word's mail merge can
do it, but it was difficult and a bit brittle.

This will take a CSV export from the accounting software and a markdown document
with templated fields + structure markup and generates an html or pdf
document, ready to print.

See the `test` directory for sample documents.
