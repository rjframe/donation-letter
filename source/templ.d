module donation_letter.templ;

import tested : test = name;
import donation_letter.parse_csv : TransactionList;

version (unittest) import donation_letter.functions : getTransactions;

/** Build an HTML file from the template. */
string build_file(const string template_path, TransactionList transactions) {
	import std.file : readText;
	return build_template(readText(template_path), transactions);
}

/** Use the provided template text to build the document. */
string build_template(const string data, TransactionList transactions) {
	import std.string : indexOf, lineSplitter;
	auto lines = data.lineSplitter;
	string[] output;

	int line_num = 0;
	while (! lines.empty) {
		auto line = lines.front;
		lines.popFront;
		line_num++;

		auto tag_begin = line.indexOf("{");
		if (tag_begin == -1) continue;

		auto tag_end = line.indexOf("}", tag_begin);
		if (tag_end == -1) {
			import std.conv : to;
			throw new Exception(
					"Missing closing brace on line " ~ line_num.to!string);
		}

		auto tag = line[tag_begin .. tag_end+1].remove_whitespace;
		switch (tag[1]) {
			case '#': // Generator.
				// I may regret not doing separate parse/build steps once I get
				// to this.
				break;
			case '=': // Function.
				import std.array : replace;
				output ~= replace(line, tag_begin, tag_end+1,
						do_function(tag[0 .. $], line_num, transactions));
				break;
			default: // Column data substitution.
				import std.array : replace;
				output ~= replace(line, tag_begin, tag_end+1,
						substitute(line[tag_begin+1 .. tag_end], transactions));
				break;
		}
	}
	import std.array : join;
	return output.join("");
}

@test("build_template properly substitutes GETYEAR function calls.")
unittest {
	assert(build_template("Some {=GETYEAR(Date,2,4)} text.", getTransactions())
			== "Some 2003 text.");
	assert(build_template("Some {=GETYEAR(Date,2,2)} text.", getTransactions())
			== "Some 03 text.");
	assert(build_template("Some{=GETYEAR(Date,2,2)}text.", getTransactions())
			== "Some03text.");
}

@test("build_template properly substitutes CSV data.")
unittest {
	assert(build_template("Some {\tAmount} text.", getTransactions())
			== "Some 100.18 text.");
	assert(build_template("Some{Date }text.", getTransactions())
			== "Some02/03/2003text.");
	assert(build_template("Some\t{ Name }  text.", getTransactions())
			== "Some\tPerson A  text.");
}

/** Remove all whitespace from the given string. */
pure nothrow
private string remove_whitespace(const string input) {
	import std.uni : isWhite;
	char[] new_string;
	foreach (ch; input) {
		if (ch.isWhite) continue;
		new_string ~= ch;
	}
	return cast(string)new_string;
}

nothrow
@test("remove_whitespace removes spaces.")
unittest {
	assert("  Some text ".remove_whitespace == "Sometext");
}

nothrow
@test("remove_whitespace removes tabs.")
unittest {
	assert("\t\tSome\t\t\ttext\t".remove_whitespace == "Sometext");
}

nothrow
@test("remove_whitespace removes newlines.")
unittest {
	assert("\n\nSome\ntext\n\n\n".remove_whitespace == "Sometext");
}

nothrow
@test("remove_whitespace removes a mixture of whitespace elements.")
unittest {
	assert(" \n\tSome\n \n\t \t text\t\n ".remove_whitespace == "Sometext");
}

/** Execute the specified function and return the result. */
private string do_function(
		const string func,
		const int line,
		TransactionList transactions)
in {
	assert(line > 0);
} body {
	// First remove the braces and split the function from the arguments.
	import std.algorithm.searching : findSplitBefore;
	import std.string : split;
	auto func_name = func.remove_whitespace[2 .. $-1].findSplitBefore("(");
	auto args = func_name[1][1 .. $-1].split(",");

	// Now we execute the desired function.
	import donation_letter.functions;
	try {
		if (func_name[0] == "GETYEAR") {
			import std.conv : to;

			if (args.length != 3) {
				throw new Exception("GETYEAR requires three arguments.");
			}
			return get_year(transactions, args[0], args[1].to!int, args[2].to!int);
		} else if (func_name[0] == "GROUPBY") {
			if (args.length != 1) {
				throw new Exception("GROUPBY requires one argument.");
			}
			return group_by(transactions, args[0]);
		} else if (func_name[0] == "SUM") {
			if (args.length != 1) {
				throw new Exception("SUM requires one argument.");
			}
			return sum(transactions, args[0]);
		} else if (func_name[0] == "TODAY") {
			if (args.length == 0) {
				return today();
			} else if (args.length == 1) {
				return today(args[0]);
			} else {
				throw new Exception("TODAY requires zero or one arguments.");
			}
		} else {
			throw new Exception("Unrecognized function " ~ func_name[0]);
		}
	} catch (Exception ex) {
		import std.conv : to;
		throw new Exception("Error on line " ~ line.to!string ~ ": " ~ ex.msg);
	}
	assert(0);
}

@test("do_function(GETYEAR) returns the year in the proper format.")
unittest {
	assert(do_function("{=GETYEAR(Date, 2, 4)}", 1, getTransactions()) == "2003");
	assert(do_function("{=GETYEAR(Date, 2, 2)}", 1, getTransactions()) == "03");
	assert(do_function("{=GETYEAR(Date, 5, 4)}", 1, getTransactions()) == "1997");
	assert(do_function("{=GETYEAR(Date, 5, 2)}", 1, getTransactions()) == "97");
}

@test("do_function(GETYEAR) doesn't care about whitespace.")
unittest {
	assert(do_function("{=GETYEAR(Date,2,2)}", 1, getTransactions()) == "03");
	assert(do_function("{   =GETYEAR  (Date, 2, 2)}", 1,
				getTransactions()) == "03");
	assert(do_function("{=GETYEAR( Date , 2, 2)}", 1, getTransactions()) == "03");
	assert(do_function("{=GETYEAR(	Date	, 2, 2)}", 1,
				getTransactions()) == "03");
	assert(do_function("{=GETYEAR(Date, 2, 2) }", 1, getTransactions()) == "03");
	assert(do_function("{=GETYEAR(Date, 2, 2  )}", 1, getTransactions()) == "03");
	assert(do_function(" {=GETYEAR(Date, 2, 2)}  ", 1, getTransactions()) == "03");
	assert(do_function(" { =	GETYEAR(Date, 2, 2)}  ", 1,
				getTransactions()) == "03");
}

@test("do_function properly calls SUM.")
unittest {
	assert(do_function("{=SUM(Amount)}", 1, getTransactions()) == "402.74");
}

// TODO: unit tests for TODAY - with and without arguments.
@test("do_function properly calls TODAY.")
unittest {
	import donation_letter.functions : today;
	assert(do_function("{=TODAY()}", 1, getTransactions()) == today());
}

@test("do_function throws an Exception when given an unknown function.")
unittest {
	import std.exception : assertThrown;
	assertThrown!Exception(do_function("{=DOESNOTEXIST()}", 1, getTransactions));
	assertThrown!Exception(do_function("{=DOESNOTEXIST(Date, 1)}", 1,
			getTransactions));
	assertThrown!Exception(do_function("{=DOESNOTEXIST(73)}", 1, getTransactions));
	assertThrown!Exception(do_function("{=DOESNOTEXIST(Amount)}", 1,
			getTransactions));
}

private string substitute(
		const string column,
		TransactionList transactions,
		const int row_index = 2)
in {
	import std.range : walkLength;
	assert(row_index > 0);
	assert(transactions.dup[].walkLength >= row_index);
} body {
	import std.algorithm.searching : canFind, countUntil;
	import donation_letter.parse_csv : Column;

	const col = column.remove_whitespace;
	if (! transactions.front[].canFind(Column(col))) {
		throw new Exception("Cannot find the specified column to insert data.");
	}

	import std.range.primitives : popFrontN;
	auto tlist = transactions[];
	const col_pos = tlist.front[].countUntil(Column(col));
	if (tlist.popFrontN(row_index-1) != row_index - 1) {
		// Horrible error message.
		throw new Exception("Not enough rows to return.");
	}
	return tlist.front[col_pos].value;
}

@test("substitute replaces text correctly with the default index.")
unittest {
	import donation_letter.functions : getTransactions;
	auto transactions = getTransactions();
	assert(substitute("Name", transactions) == "Person A");
	assert(substitute("Date", transactions) == "02/03/2003");
}

@test("substitute replaces text correctly with non-default indices.")
unittest {
	import donation_letter.functions : getTransactions;
	auto transactions = getTransactions();
	assert(substitute("Name", transactions, 3) == "Person B");
	assert(substitute("Amount", transactions, 4) == "100.08");
}

/* This can only happen in buggy code, and there's a contract guarding for this.

@test("substitute throws an Exception if the specified index is out of bounds.")
unittest {
	import donation_letter.functions : getTransactions;
	import std.exception : assertThrown;
	auto transactions = getTransactions();
	assertThrown(substitute("Name", transactions, 17));
}
*/
