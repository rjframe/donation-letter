/** The list and implementation of functions callable from templates.

	The group_by function allows the caller to split the TransactionList[string]
	into the desired TransactionList for the other functions to use.
 */
module donation_letter.functions;

import donation_letter.parse_csv : TransactionList;

import tested : test = name;

version(unittest) {
	import donation_letter.parse_csv : Row;

	@safe nothrow
	TransactionList getTransactions() {
		auto transactions = TransactionList();
		transactions.insert(Row("1", "Person A", "02/03/1997", "100.18"));
		transactions.insert(Row("1", "Person A", "01/02/03", "100.08"));
		transactions.insert(Row("2", "Person B", "01/02/03", "102.30"));
		transactions.insert(Row("1", "Person A", "02/03/2003", "100.18"));
		transactions.insert(Row("ID", "Name", "Date", "Amount"));

		return transactions;
	}
}

/** Template function GETYEAR([column-name], [row-number], [number of digits (2|4))

	The row is a 1-based index (the 0th index is the header row).
	2-digit year input is assumed to be 20xx.
*/
@safe
string get_year(
		TransactionList transactions,
		const string column,
		const int row,
		const int num_digits) {
	if (num_digits != 2 && num_digits != 4) {
		throw new Exception(
				"The number of digits to display the year must be 2 or 4.");
	}

	import std.algorithm.searching : countUntil;
	import std.array : split;
	import std.range.primitives : popFrontN;
	import donation_letter.parse_csv : Column;

	auto tlist = transactions[];
	auto headers = tlist.front;
	const date_col = headers[].countUntil(Column(column));

	tlist.popFrontN(row-1);
	string full_date = tlist.front[date_col].value;

	// TODO: I need to be able to handle at least:mm/dd/yy, mm-dd-yy, ISO.
	// TODO: The format needs to be configurable, to handle mm/dd/yy and yy/mm/dd.
	string date = full_date.split("/")[2];

	if (date.length == num_digits) return date;
	if (date.length == 2 && num_digits == 4) return "20" ~ date;
	return date[2 .. $];
}

@safe
@test("get_year returns the correct year in the proper format.")
unittest {
	auto transactions = getTransactions();
	assert(transactions.get_year("Date", 2, 2) == "03");
	assert(transactions.get_year("Date", 2, 4) == "2003");
	assert(transactions.get_year("Date", 4, 2) == "03");
	assert(transactions.get_year("Date", 4, 4) == "2003");
	assert(transactions.get_year("Date", 5, 2) == "97");
	assert(transactions.get_year("Date", 5, 4) == "1997");
}

/** Template function GROUPBY([column-name])

	Returns column-name, or throws an exception if the column doesn't exist.
*/
@safe pure
string group_by(TransactionList transactions, const string column) {
	import std.algorithm.searching : canFind;
	import donation_letter.parse_csv : Column;
	if (transactions.front[].canFind(Column(column))) {
		return column;
	} else throw new Exception("The GROUPBY column is not in the CSV.");
}

@safe
@test("group_by returns the header field name by which to group functions.")
unittest {
	assert(group_by(getTransactions(), "ID") == "ID");
	assert(group_by(getTransactions(), "Name") == "Name");
}

@safe
@test("group_by throws an Exception if the specified group doesn't exist.")
unittest {
	import std.exception : assertThrown;
	assertThrown(group_by(getTransactions(), "Not here"));
}

/** Template function SUM([column-name])

	Iterates all values in the specified column and returns the sum.
*/
@safe
string sum(TransactionList transactions, const string column) {
	import std.algorithm.searching : canFind;
	import donation_letter.parse_csv : Column;
	if (! transactions.front[].canFind(Column(column))) {
		throw new Exception("SUM cannot find the specified column.");
	}

	int index;
	auto columns = transactions.front[];
	for (index = 0; index < columns.length; index++) {
		if (columns[index] == Column(column)) break;
	}
	transactions.removeFront();

	import std.conv : to;
	double total = 0;
	foreach (transaction; transactions[]) {
		total += transaction[index].value.to!double;
	}
	return total.to!string;
}

@safe
@test("sum returns the correct total.")
unittest {
	assert(sum(getTransactions(), "Amount") == "402.74");
}

@safe
@test("sum throws an exception if the specified group does not exist.")
unittest {
	import std.exception : assertThrown;
	assertThrown(sum(getTransactions(), "Not here"));
}

@safe
@test("sum throws an exception if the specified group is not numerical.")
unittest {
	import std.exception : assertThrown;
	assertThrown(sum(getTransactions(), "Name"));
}


/** Template function TODAY([format-string] = "dd/mmm/yyyy")

	Returns today's date in the specified format.
*/
@safe
string today(const string format="dd/m/yyyy") {
	import std.datetime : Clock;
	return today(Clock.currTime, format);
}

import std.datetime : SysTime;
/** See the public function for documentation; this allows injecting a date
	for testing.
*/
@safe
private string today(const SysTime now, const string format="dd/m/yyyy") {
	import std.array : appender;
	import std.conv : to;
	import std.format : formattedWrite;

	auto writer = appender!string();
	formattedWrite(writer, "%s/%s/%s", now.day, now.month.to!int, now.year);
	return writer.data.dup;
}

@safe
@test("today's public implementation returns today's date.")
unittest {
	import std.datetime : Clock;

	auto now = Clock.currTime;
	assert(today() == today(now));
}

@safe
@test("today returns today's date with the default formatting.")
unittest {
	import std.datetime : Date, SysTime;

	auto now = SysTime(Date(2017, 3, 27));
	assert(today(now) == "27/3/2017");
	now = SysTime(Date(1804, 12, 31));
	assert(today(now) == "31/12/1804");
}

@safe
@test("today returns today's date with custom formatting.")
unittest {
	import std.datetime : Date, SysTime;

	auto now = SysTime(Date(2017, 3, 27));
	assert(today(now, "mm/dd/yy") == "03/27/17");
	now = SysTime(Date(1804, 12, 31));
	assert(today(now, "mm/dd/yyyy") == "12/31/1804");
}

@safe
@test("today throws an Exception if it doesn't understand the format string.")
unittest {
	assert(0);
}

// TODO: something to transform date formats in case the template needs
// something other than what the accounting package exports.
