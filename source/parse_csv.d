module donation_letter.parse_csv;

import std.container.slist : SList;
import tested : test = name;

/** A Column represents a column in a CSV file.

	The Column contains a single cell and is contained in a Row. The Rows must
	be iterated to obtain the data from the entire column.
*/
struct Column {
	private string data;

	/** Construct a new Column with the specified data. */
	@safe pure nothrow @nogc
	this(const string str) { data = str; }

	/** Retrieve the data stored in the Column. */
	@property
	@safe nothrow @nogc
	string value() { return data; }

	@safe pure nothrow @nogc
	ref Column opAssign(const string str) {
		data = str;
		return this;
	}

	@safe nothrow @nogc
	@test("Column can be assigned from string.")
	unittest {
		Column col2;
		auto col = col2 = "asdf";
		assert(col.data == "asdf");
		assert(col2.data == "asdf");
	}

	@safe pure nothrow @nogc
	bool opEquals()(auto ref const string str) const {
		return (data == str);
	}

	@test("Column can be tested for equality with string.")
	unittest {
		auto c1 = Column("asdf");

		@safe nothrow @nogc
		void func() {
			assert(c1 == "asdf");
			assert(c1 != "sdfg");
		}
		func();
	}

	@safe pure nothrow @nogc
	bool opEquals()(auto ref const Column other) const {
		return data == other.data;
	}

	@test("Column can be tested for equality with other Column.")
	unittest {
		auto c1 = Column("asdf");
		auto c2 = Column("asdf");
		auto c3 = Column("sdfg");

		@safe nothrow @nogc
		void func() {
			assert(c1 == c2);
			assert(c1 != c3);
		}
		func();
	}
}

/** A Row object corresponds to a row in a CSV file. */
struct Row {
	private Column[] cols;

	/** Construct a Row containing the specified list of Columns. */
	@safe nothrow
	this(string[] columns ...) {
		for (int i = 0; i < columns.length; i++) {
			this.cols ~= Column(columns[i]);
		}
	}

	@safe nothrow
	@test("Row can be instantiated from array of string.")
	unittest {
		auto row = Row("1", "2", "3");
		assert(row.cols[0] == "1");
		assert(row.cols[1] == "2");
		assert(row.cols[2] == "3");
	}

	bool opEquals()(auto ref const Row other) const {
		import std.algorithm.comparison : isPermutation;
		return cols.isPermutation(other.cols);
	}

	@test("Row can be tested for equality with other Row.")
	unittest {
		auto r1 = Row("a", "b", "c", "d");
		auto r2 = Row("a", "b", "c", "d");
		auto r3 = Row("a", "b", "d", "c");
		auto r4 = Row("a", "b", "c");

		@safe nothrow @nogc
		void func() {
			assert(r1 == r2);
			assert(r1 == r3);
			assert(r1 != r4);
		}
		func();
	}

	@safe pure @nogc
	ref Column opIndex(const uint index) { return cols[index]; }

	@test("Row can return a Column via its index.")
	unittest {
		auto row = Row();
		row.cols ~= Column("asdf");
		row.cols ~= Column("sdfg");

		@safe @nogc
		void func() {
			assert(row[0].data == "asdf");
			assert(row[1].data == "sdfg");
		}
		func();
	}

	@safe pure nothrow @nogc
	Column[] opIndex() { return cols[]; }

	@test("Row can return its columns as a slice.")
	unittest {
		Row row;
		row.cols ~= Column("one");
		row.cols ~= Column("two");

		@safe nothrow @nogc
		void func() {
			auto r = row[];
			assert(r[0].data == "one");
			assert(r[1].data == "two");
		}
		func();
	}

	@safe pure nothrow
	ref Row opOpAssign(string op)(const Row other) if (op == "~") {
		cols ~= other.cols;
		return this;
	}

	@test("Row can append another Row's columns to its own.")
	unittest {
		auto r1 = Row();
		auto r2 = Row();
		r1.cols ~= [Column("one"), Column("two"), Column("three")];
		r2.cols ~= [Column("One"), Column("Two")];

		@safe nothrow void func() { r1 ~= r2; }
		func();

		assert(r1.cols.length == 5);
		assert(r1.cols[2].data == "three");
		assert(r1.cols[3].data == "One");
		assert(r1.cols[4].data == "Two");
	}

	@safe pure nothrow
	ref Row opOpAssign(string op)(const Column col) if (op == "~") {
		cols ~= col;
		return this;
	}

	@safe nothrow
	@test("Row can append a Column to its list of Columns.")
	unittest {
		auto row = Row();
		auto col = Column("asdf");
		row ~= col;
		assert(row.cols[0].data == "asdf");
	}

	@safe pure nothrow
	ref Row opOpAssign(string op)(const string col) if (op == "~") {
		cols ~= Column(col);
		return this;
	}

	@safe nothrow
	@test("Row can append a string to its list as a Column.")
	unittest {
		Row row;
		row ~= "asdf";
		assert(row.cols[0].data == "asdf");
	}

	struct RowRange {
		private Row row;

		@safe nothrow @nogc
		this(Row row) { this.row = row; }

		@safe @nogc
		@property Column front() {
			return row[0];
		}

		@safe
		@test("RowRange.front() returns the first element.")
		unittest {
			auto row = Row("a", "b", "c");
			assert(row.range().front == Column("a"));
		}

		@safe @nogc
		@property bool empty() {
			return (row.cols.length ==0);
		}

		@test("RowRange.empty() returns true iff the range has no elements.")
		unittest {
			auto row = Row();

			@safe @nogc
			void test(bool expected) {
				assert(row.range().empty == expected);
			}

			test(true);
			row ~= Column("a");
			test(false);
		}

		@safe @nogc
		void popFront() {
			row.cols = row.cols[1 .. $-1];
		}

		@test("RowRange.popFront() removes the first element.")
		unittest {
			auto row = Row("a", "b", "c");
			auto rng = row.range();

			@safe @nogc
			void func() {
				assert(rng.front == Column("a"));
				rng.popFront();
				assert(rng.front == Column("b"));
			}
			func();
		}
	}

	@safe nothrow @nogc
	auto range() {
		return RowRange(this);
	}

	@test("Ensure the RowRange leaves the underlying Row untouched.")
	unittest {
		auto row = Row("a", "b", "c", "d");
		auto rng = row.range();
		rng.popFront; rng.popFront;

		assert(row[0] == Column("a"));
		assert(row[1] == Column("b"));
		assert(row[2] == Column("c"));
		assert(row[3] == Column("d"));
	}
}

alias TransactionList = SList!(Row);

pure
TransactionList[string] parse_text(const string text, const string group_by) {
	import std.algorithm.searching : countUntil;
	import std.conv : to;
	import std.csv : csvReader;

	// The transactions.header array will contain the header row.
	auto transactions = csvReader!(string[string])(text, null);
	auto group_index = transactions.header.countUntil(group_by);
	if (group_index == -1) throw new Exception("Invalid group_by specified.");

	TransactionList[string] sortedTransactions;
	foreach (t; transactions) {
		if (! (t[transactions.header[group_index]] in sortedTransactions)) {
			sortedTransactions[t[transactions.header[group_index]]]
					= TransactionList();
		}

		Row row;
		foreach (col; t) { row ~= col; }
		sortedTransactions[t[transactions.header[group_index]]]
				.insertFront(row);
	}

	// We'll add the headers as our first row.
	// This is duplicative, but makes the template functions easier.
	auto headers = Row();
	for (int i = 0; i < transactions.header.length; i++) {
		headers ~= transactions.header[i];
	}
	foreach (group; sortedTransactions.keys) {
		sortedTransactions[group].insertFront(headers);
	}

	return sortedTransactions;
}

@test("parse_text returns a correct AA of TransactionList.")
unittest {
	auto csv = "ID,Name,Date,Amount\n1,Person A,01/02/03,100.08\n" ~
			"2,Person B,02/03/03,120.00\n1,Person A,01/03/03,34.34\n";
	// These are the check values.
	auto person_a_header = Row("ID", "Name", "Date", "Amount");
	auto person_a_1 = Row("1", "Person A", "01/02/03", "100.08");
	auto person_a_2 = Row("1", "Person A", "01/03/03", "34.34");
	auto person_b_headers = Row("ID", "Name", "Date", "Amount");
	auto person_b_1 = Row("2", "Person B", "02/03/03", "120.00");

	auto transactions = parse_text(csv, "ID");

	auto person_a = transactions["1"];
	assert(person_a.front == person_a_header);
	person_a.removeFront;
	assert(person_a.front == person_a_2);
	person_a.removeFront;
	assert(person_a.front == person_a_1);

	auto person_b = transactions["2"];
	assert(person_b.front == person_b_headers);
	person_b.removeFront;
	assert(person_b.front == person_b_1);
}

/** Parse the specified file.

	Returns an AA of TransactionList; each key in the AA is an identifier from
	the `group_by` column.
*/
TransactionList[string] parse_file(const string path, const string group_by) {
	import std.file : readText;
	return parse_text(readText(path), group_by);
}

